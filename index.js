const express = require('express');
const app = express();
const fetch = require('node-fetch');
const PORT = process.env.PORT || 8000;

const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));

app.set('view engine', 'hbs');

app.use(express.static(__dirname + '/public'));

let latestData = [];
let customSearchResults = [];
let searchHeadline = "";
let offsetCount = 0;
 
let search = (topic="", offset=0) => {
    return{
        "queryString": topic,
        "resultContext" : {
            "maxResults": 20,
            "offset": offset,
            "aspects" :[  "title","lifecycle","location","summary","editorial" ]
        }
    }
}

getFT= () => {
    fetch('http://api.ft.com/content/search/v1', {
        method: 'POST',
        body: JSON.stringify(search()), 
        headers:{
        'X-Api-Key': process.env.API_KEY,
        'Content-Type': 'application/json'
        }
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(res => latestData.push(res.results))
}

getFT();

app.get('/', function(req, res){
    let searchResults = latestData[0][0].results;
    let resetSearch = () => {
        searchHeadline = "";
    }
    resetSearch();
    res.render('home', {
        show_results: searchResults,
        heading: "Latest headlines",
        page: false
    });
})

app.post('/',function(req, res){
    let userSearch = req.body;
    searchHeadline = `title:\"${userSearch.search}\"`;

    fetch('http://api.ft.com/content/search/v1', {
        method: 'POST',
        body: JSON.stringify(search(searchHeadline)),
        headers:{
        'X-Api-Key': process.env.API_KEY,
        'Content-Type': 'application/json'
        }
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(function(res){
        return customSearchResults.push(res.results);
    })
    .then(function(){
        let latestSearch = customSearchResults[0][0].results;
        res.render('home', {
            show_results: customSearchResults[customSearchResults.length-1][0].results,
            heading: "Search results for " + "'" + userSearch.search + "'"
        })
    })
});

app.get('/nextpage',function(req, res){
    let userSearch = req.body;
    let nextPage = () => {
        offsetCount += 20;
        return offsetCount;
    }

    let searchCheck = () => {
        if(searchHeadline){
            return "Search results for " + "'" + searchHeadline.match( /"(.*?)"/ )[1] + "'";
        } else {
            return "Latest headlines"
        }
    }

    let showPrevPage = () => {
        if(offsetCount === 0){
            return false
        } else {
            return true
        }
    }

    fetch('http://api.ft.com/content/search/v1', {
        method: 'POST',
        body: JSON.stringify(search(searchHeadline, nextPage())),
        headers:{
        'X-Api-Key': process.env.API_KEY,
        'Content-Type': 'application/json'
        }
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(function(res){
        return customSearchResults.push(res.results);
    })
    .then(function(){
        let latestSearch = customSearchResults[0][0].results;
        res.render('home', {
            show_results: customSearchResults[customSearchResults.length-1][0].results,
            heading: searchCheck(),
            page: showPrevPage()
        })
    })
});

app.get('/previouspage',function(req, res){
    let userSearch = req.body;
    let prevPage = () => {
        if(offsetCount === 0){
            return offsetCount = 0;
        } else if(offsetCount > 0){
            return offsetCount-=20;
        }
    }

    let searchCheck = () => {
        if(searchHeadline){
            return "Search results for " + "'" + searchHeadline.match( /"(.*?)"/ )[1] + "'";
        } else {
            return "Latest headlines"
        }
    }

    let showPrevPage = () => {
        if(offsetCount === 0){
            return false
        } else {
            return true
        }
    }

    fetch('http://api.ft.com/content/search/v1', {
        method: 'POST',
        body: JSON.stringify(search(searchHeadline, prevPage())),
        headers:{
        'X-Api-Key': process.env.API_KEY,
        'Content-Type': 'application/json'
        }
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(function(res){
        return customSearchResults.push(res.results);
    })
    .then(function(){
        let latestSearch = customSearchResults[0][0].results;
        res.render('home', {
            show_results: customSearchResults[customSearchResults.length-1][0].results,
            heading: searchCheck(),
            page: showPrevPage()
        })
    })
});

app.listen(PORT, function(){
    console.log("Listening on port 8000!")
})
