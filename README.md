# FT app
This web app was built to serve FT readers and provide them with the latest headlines. It utilises the FT Developer API. 

It includes:

 * Search functionality
 * Pagination of 20 results per page
 * Responsive design, whereby it has been optimised for desktop, tablet and mobile devices  

All crafted with the following stack:

 * HTML
 * CSS
 * JavaScript
 * Node.js

Also, the app utilises server-side rendering (via Handlebars) and incorporates Origami Components for the FT look and feel.

It is deployed to Heroku - check it out @ https://ft-afullerton.herokuapp.com/ !
